extends TileMap

var currentPlayer: PlayerCharacter
var drowned = false
var sensor = Vector2(0, 0)

var splashScene = preload("res://Scenes/StageComponents/Waters/Splash.tscn")
var splashable = true

var playerAlive = true

func splash():
	if !splashable:
		return
	splashable = false
	var splash = splashScene.instance()
	splash.position = currentPlayer.position + sensor
	call_deferred('add_child', splash)
	$SplashTimer.start()

func _physics_process(_d):
	if (currentPlayer != null):
		var prevDrowned = drowned
		var id = get_cellv(world_to_map(currentPlayer.position + sensor))
		if id == 0:
			drowned = true
		else:
			drowned = false
		
		if !playerAlive:
			return
		if !prevDrowned and drowned:
			if currentPlayer.type == 'brimstone' || currentPlayer.type == 'brimstone_charged':
				playerAlive = false
			currentPlayer.drown()
			splash()
			sensor = Vector2(0, -16)
		elif prevDrowned and !drowned:
			currentPlayer.undrown()
			splash()
			sensor = Vector2(0, 0)
		
		elif id == 1:
			if currentPlayer.type == 'salt':
				currentPlayer.changeType('salt_purified')
			splash()
	else:
		drowned = false


func _on_SplashTimer_timeout():
	splashable = true

func _on_PlayerGenerator_restart(player):
	currentPlayer = player
	playerAlive = true
