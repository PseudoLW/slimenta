extends Node2D

export var lifetime = 0.5
signal done

func _ready():
	$Sprite.scale = Vector2(0, 0)
	$Timer.start(lifetime)
	$Particles.lifetime = lifetime
	$Particles.emitting = true

func remove_particle():
	$Particles.queue_free()

func _process(_d):
	var scale = 1 - $Timer.time_left / lifetime
	$Sprite.scale = Vector2(scale, scale)
	$Sprite.modulate.a = 1 - scale

func _on_Timer_timeout():
	emit_signal("done")
	queue_free()
