extends TileMap

var state;
var greenTile;
var purpleTile;
var switchTile;
var resetState;

class_name SwitchBlock

const tileShape = []
const tileXindex = {}
const switchXindex = {}

func _ready():
	# It's a shame that godot doesn't support funcprog yet :/
	for i in tile_set.get_tiles_ids():
		if tile_set.tile_get_name(i) == 'GreenBlock':
			greenTile = i
			break
	for i in tile_set.get_tiles_ids():
		if tile_set.tile_get_name(i) == 'PurpleBlock':
			purpleTile = i
			break
	for i in tile_set.get_tiles_ids():
		if tile_set.tile_get_name(i) == 'Switch':
			switchTile = i
			break
			
	var poly = ConvexPolygonShape2D.new()
	poly.set_points(PoolVector2Array([
		Vector2(0, 0),
		Vector2(0, 32),
		Vector2(32, 32),
		Vector2(32, 0)]))
	tileShape.append({
		"autotile_coord": Vector2(0, 0),
		"one_way": false,
		"one_way_margin": 1,
		"shape": poly,
		"shape_transform": [Vector2(1, 0), Vector2(0, 1), Vector2(0, 0)]
	})
	tile_set.tile_set_shapes(greenTile, tileShape)
	tileXindex[greenTile] = 96
	tileXindex[purpleTile] = 64
	switchXindex[greenTile] = 0
	switchXindex[purpleTile] = 32
	resetState = greenTile
	state = resetState

func bump(v):
	var r = Vector2(1, 0)
	if (
		!(get_cellv(v) == switchTile or 
		get_cellv(v + r) == switchTile or 
		get_cellv(v - r) == switchTile) or
		$Cooldown.time_left > 0
	):
		return
	swapState()

func swapState():
	$Cooldown.start()
	$AudioStreamPlayer.play()
	var newState;

	if (state == greenTile):
		newState = purpleTile
	elif (state == purpleTile):
		newState = greenTile

	tile_set.call_deferred(
		'tile_set_region',
		state, 
		Rect2(tileXindex[state], 64 + 32, 32, 32)
	)
	tile_set.call_deferred(
		'tile_set_region',
		newState,
		Rect2(tileXindex[newState], 64, 32, 32)
	)
	tile_set.call_deferred(
		'tile_set_region',
		switchTile, 
		Rect2(switchXindex[newState], 64, 32, 32)
	)
	tile_set.call_deferred(
		"tile_set_shapes",
		state, []
	)
	tile_set.call_deferred(
		"tile_set_shapes",
		newState, tileShape
	)
	
	state = newState

func _on_PlayerGenerator_restart(_r):
	if (state != resetState):
		swapState()
