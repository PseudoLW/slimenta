extends Node2D

const explosion2 = preload("res://Scenes/Particles/Explosion.tscn")

var velocity = Vector2(0, 0)
var minVel = 10
var player

func _ready():
	$Explosion.remove_particle()
	$Timer.start()
	$Sound.play()

func _process(delta):
	position += velocity * delta
	velocity *= 0.8
	
	if (has_node("Sprite")):
		$Sprite.scale = Vector2($Timer.time_left/1.5, $Timer.time_left/1.5)
		$Sprite.position.y = -17*$Timer.time_left/1.5
	if velocity.length() < minVel:
		velocity = Vector2(0, 0)

func setSpriteType(type):
	$Sprite.play(type + "_jump")
	$Sprite.set_frame(1)
	$Camera.current = true


func _on_Timer_timeout():
	$Sprite.queue_free()
	player.queue_free()
	var explosion = explosion2.instance()
	add_child(explosion)
