extends CanvasLayer

var time = 0
var startTime = 0
var timerRunning = true
var winning = false

export var levelName = "aaaaa"
signal done
signal hardReset

const totalTime = 3

func resetTimer():
	$Score.add_color_override("font_color", Color(1, 1, 1, 1))
	startTime = OS.get_ticks_msec()
	timerRunning = true

func _ready():
	resetTimer()

func changeText(t):
	$LevelName.text = t

func _process(_d):
	if (winning):
		$WinningPanel/ProgressBar.value = 1 - $Timer.time_left / totalTime
		if (Input.is_action_just_pressed("ui_cancel")):
			emit_signal("hardReset")
			winning = false
			$WinningPanel.visible = false
			$Timer.stop()
			$Score.show()
			resetTimer()
			
	if (!timerRunning):
		return
	time = OS.get_ticks_msec() - startTime
	var seconds = time / 1000
	var milisecs = time - seconds * 1000
	var minute = seconds / 60
	seconds -= minute * 60
	$Score.text = '%02d.%02d.%03d' % [minute, seconds, milisecs]

func stopTimer():
	timerRunning = false


func _on_PlayerGenerator_death_occurred():
	$Score.add_color_override("font_color", Color(1, 0, 0, 1))
	stopTimer()


func _on_PlayerGenerator_restart(_d):
	resetTimer()


func _on_LevelEnd_body_entered(_b):
	$Score.visible = false
	winning = true
	stopTimer()
	var seconds = time / 1000
	var milisecs = time - seconds * 1000
	var minute = seconds / 60
	seconds -= minute * 60
	$WinningPanel.visible = true
	$Timer.start(totalTime)
	$WinningPanel/Label.text = '%02d.%02d.%03d' % [minute, seconds, milisecs]


func _on_Timer_timeout():
	emit_signal('done')
