extends Node2D

var charaScene = load("res://Scenes/StageComponents/Player/Player.tscn")
var currentPlayer
var winning = false

signal restart
signal death_occurred

func _ready():
	print(position)
	$Hint.queue_free()

func _physics_process(_d):
	if not is_instance_valid(currentPlayer) and not winning:
		currentPlayer = charaScene.instance()
		currentPlayer.position = position
		emit_signal('restart', currentPlayer)
		currentPlayer.connect('death_occurred', self, "death")
		currentPlayer.connect('win', self, "win")
		get_tree().get_root().call_deferred("add_child", currentPlayer)

func win():
	winning = true

func death():
	emit_signal("death_occurred")
