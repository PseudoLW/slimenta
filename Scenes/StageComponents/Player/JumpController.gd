extends Node

signal grounded_jump_press
signal airborne_jump_press
signal double_jump_press
signal jump_release

export (float) var buffer_duration = 10; # / 60 seconds
export (float) var double_jump_duration = 20; # / 60 seconds

var airborne = true

func set_able_to_jump():
	airborne = false

func set_unable_to_jump():
	airborne = true

func _physics_process(_d):
	if Input.is_action_just_pressed("main_input"):
		$JumpBufferTimer.start(buffer_duration / 60)
		if airborne:
			if $DoubleJumpTimer.time_left > 0:
				emit_signal("double_jump_press")
			else:
				emit_signal("airborne_jump_press")

	if Input.is_action_just_released("main_input"):
		$JumpBufferTimer.stop()
		emit_signal("jump_release")

	if $JumpBufferTimer.time_left > 0 and not airborne:
		airborne = true
		$JumpBufferTimer.stop()
		$DoubleJumpTimer.start(double_jump_duration / 60)
		emit_signal("grounded_jump_press")
