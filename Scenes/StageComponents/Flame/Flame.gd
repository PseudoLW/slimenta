extends Area2D

func _on_Flame_body_entered(body):
	if (body is PlayerCharacter):
		if (body.type == "brimstone"):
			$AudioStreamPlayer.play()
			body.changeType('brimstone_charged')
		elif (body.type != 'brimstone_charged'):
			body.kill()
