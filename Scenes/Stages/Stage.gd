extends Node2D

export var stageName = 'Stage Test'
signal win

func changeText(t):
	$HUD.changeText(t)


func _on_HUD_done():
	emit_signal('win')


func _on_HUD_hardReset():
	$PlayerGenerator.currentPlayer.queue_free()
