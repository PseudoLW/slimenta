extends Node2D

const radius = 92
const blocksize = 32

# Talking about code smell, eh?
func _on_Area2D_body_entered(body):
	if (body is SwitchBlock):
		var switch = body.switchTile
		for x in range(0, radius + blocksize, blocksize):
			for y in range(0, radius + blocksize, blocksize):
				if x * x + y * y <= radius * radius:
					for combo in [[x, y], [x, -y], [-x, y], [-x, -y]]:
						var pos = global_position + Vector2(combo[0], combo[1])
						if (body.get_cellv(body.world_to_map(pos)) == switch):
							body.swapState()
							return


func _on_Explosion_done():
	queue_free()
