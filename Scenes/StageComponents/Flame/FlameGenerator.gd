extends TileMap

var flameScene = preload("res://Scenes/StageComponents/Flame/Flame.tscn")

const positions = [null, null, null];
const instances = []
const consumed = []

func _ready():
	for position in get_used_cells():
		var transp = is_cell_transposed(position.x, position.y)
		var flipx = is_cell_x_flipped(position.x, position.y)
		var flipy = is_cell_y_flipped(position.x, position.y)
		
		var flame = flameScene.instance()
		get_tree().get_root().call_deferred("add_child", flame)

		var angle
		if (!transp && !flipx && !flipy):
			angle = 0
		elif (transp && flipx && !flipy):
			angle = PI / 2
		elif (!transp && flipx && flipy):
			angle = 2 * PI / 2
		elif (transp && !flipx && flipy):
			angle = 3 * PI / 2
		else:
			assert(false, "Invalid parity")
		flame.rotate(angle)
		flame.translate(Vector2(16, 0).rotated(angle) + Vector2(16, 16))
		flame.translate(map_to_world(position))
		instances.append(flame)

func _exit_tree():
	for flame in instances:
		if is_instance_valid(flame):
			flame.queue_free()
	instances.clear()
