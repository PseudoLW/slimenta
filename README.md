# Slimenta

Legends says that slimes are derived from classical elements. No you tho. Your friends are built on fire, water, earth, air. You are built on salt, sulphur, and mercury.

Collect the elemental totem from each levels! You might use some of the elements to your help.

## Important stuff

**There might be invisible wall, or random flame in the game. This is unintended. Please, restart the game if this happens.**

## Diversifier Description
-   **A.** "Implementasi game hanya dengan satu tombol controller."
    This game primarily uses one button only, that is ctrl. You can use arrow keys and enter on menu, tho.
-   **Elemental Mastery** "Implementasi fitur elemen-elemen dalam game."
    This game has 2 elemental type (3 was planned but sadly mercury was not implemented); namely *salt* (white; default form) the binder of water, *sulphur* (yellow) the binder of fire, and *mercury* the binder of metal.
-   **Ame ame ame** "Implementasi suatu particle effect"
    This game has particles when the slime is walking, dying, emitting explosion, plunging on water, etc.

## How To Play

### Controls
-   Arrow keys/enter for menu navigation.
-   Control to jump.

### Special Abilities
-   If you are *salt*, you can bind with water, allowing you to gain extra jump.
-   If you are *sulphur*, you can bind with fire, allowing you to gain incredible speed, and slam switch blocks from its side.

