extends CanvasLayer

const selection = ['Play', 'Exit']
var selected = 0

signal play

func play_sound():
	if $Sound.playing:
		$Sound.stop()
	$Sound.play()

func refresh_selection():
	for i in range(2):
		var selText = selection[i]
		if selected == i:
			get_node(selText + 'Label').text = '> %s <' % selText
		else:
			get_node(selText + 'Label').text = selText

func _ready():
	refresh_selection()

func _process(_d):
	if Input.is_action_just_pressed("ui_up"):
		$Sound.play()
		selected = (selected + 1) % 2
		refresh_selection()
	elif Input.is_action_just_pressed("ui_down"):
		$Sound.play()
		selected = (selected + 1) % 2
		refresh_selection()
	elif Input.is_action_just_pressed("ui_accept"):
		match selected:
			0:
				emit_signal('play')
			2:
				get_tree().quit()
