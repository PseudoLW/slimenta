extends Control

var type

func setup(newtype, number):
	type = newtype
	$AnimatedSprite.play(type)
	$Label.text = '%d' % number

func highlight():
	$AnimatedSprite.play(type + '_highlighted')

func unhighlight():
	$AnimatedSprite.play(type)
