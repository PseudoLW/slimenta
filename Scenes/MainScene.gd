extends Node2D

const MainMenu = preload('res://Scenes/GUI/MainMenu.tscn')
const LevelSelection = preload('res://Scenes/GUI/LevelSelection.tscn')
const Levels = [
	preload('res://Scenes/Stages/Stage1.tscn'),
	preload('res://Scenes/Stages/Stage2.tscn'),
	preload('res://Scenes/Stages/Stage3.tscn'),
	preload('res://Scenes/Stages/Stage4.tscn'),
	preload('res://Scenes/Stages/Stage5.tscn'),
	preload('res://Scenes/Stages/Stage6.tscn'),
	preload('res://Scenes/Stages/Stage7.tscn'),
	preload('res://Scenes/Stages/Stage8.tscn'),
	#preload('res://Scenes/Stages/Stage9.tscn'),
	#preload('res://Scenes/Stages/Stage10.tscn'),
]
const levelNames = [
	'Initiation',
	'The Hurting is not as Rewarding',
	"Don't Fall",
	'The Tower',

	'Ring of Fire',
	"Good ol' Switcheroo",
	'Into the Brimstone',

	'One Lap and a Half',
	#'Beware: Deep Water',
	#'Salty Water',
]
var currentScene
var currentStage = 0

func _ready():
	$AudioStreamPlayer.play()
	currentScene = MainMenu.instance()
	currentScene.connect("play", self, 'selectStage')
	add_child(currentScene)

func selectStage():
	currentScene.queue_free()
	currentScene = LevelSelection.instance()
	currentScene.levelNames = levelNames
	currentScene.connect("playStage", self, 'playStage')
	add_child(currentScene)

func playStage(i):
	currentStage = i
	remove_child(currentScene)
	currentScene.call_deferred('free')
	currentScene = Levels[i].instance()
	currentScene.get_node('Stage').changeText(
		'%d - %s' % [currentStage + 1, levelNames[currentStage]]
	)
	currentScene.get_node('Stage').connect('win', self, 'win')
	add_child(currentScene)

func _process(_delta):
	if Input.is_action_just_pressed("ui_focus_next"):
		print_tree_pretty()

func win():
	selectStage()
