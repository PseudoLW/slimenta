extends Area2D

var powerupType

var bobbing_distance = 5
const bobbing_duration = 80

var time = 0

func _ready():
	pass # Replace with function body.

func setPowerupType(type):
	powerupType = type
	$Sprite.play(type)

func _process(_d):
	time += 1
	if time > bobbing_duration:
		time = time - bobbing_duration
	$Sprite.position.y = sin(2 * PI * time / bobbing_duration) * bobbing_distance

func _on_LevelEnd_body_entered(body):
	if body is PlayerCharacter:
		print(get_stack())
		body.win()
