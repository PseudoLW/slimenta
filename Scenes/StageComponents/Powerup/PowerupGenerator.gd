extends TileMap

var powerupScene = preload("res://Scenes/StageComponents/Powerup/Powerup.tscn")

const names = [
	'salt',
	'quicksilver',
	'brimstone'
]

var pregenerated = false

const positions = [null, null, null];
const instances = []
const consumed = []

func _ready():
	for i in range(3):
		var arr = []
		for position in get_used_cells_by_id(i):
			arr.append(map_to_world(position) + Vector2(32, 24))

		positions[i] = arr
		tile_set.remove_tile(i)
	consumed.clear()
	generate()

func generate():
	for i in range(3):
		for position in positions[i]:
			var powerup = powerupScene.instance()
			powerup.setPowerupType(names[i])
			powerup.connect('collected', self, '_onCollectPowerup')
			call_deferred("add_child", powerup)
			powerup.position = position
			instances.append(powerup)

func regenerate():
	for item in consumed:
		var powerup = powerupScene.instance()
		powerup.setPowerupType(item['type'])
		powerup.connect('collected', self, '_onCollectPowerup')
		call_deferred("add_child", powerup)
		powerup.position = item['pos']

		

func _onCollectPowerup(type, pos):
	consumed.append({
		"type": type,
		"pos": pos
	})

func _on_PlayerGenerator_restart(_r):
	if pregenerated:
		regenerate()
		consumed.clear()
	else:
		pregenerated = true

func _exit_tree():
	for instance in instances:
		if is_instance_valid(instance):
			instance.queue_free()
	instances.clear()
