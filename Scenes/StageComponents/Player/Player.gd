extends KinematicBody2D

const jumpSpeed = -350
const gravity = 1600

const maxHorizontalSpeed = 450
const horizontalAccel = 700

const jumpGravityMultiplication = 0.08

var terminalVelocity = 1000

const UP = Vector2(0,-1)
var velocity = Vector2()
var waterTile: TileMap
var beginJump = false
var living = true

var gravMult = 1

var touchingJumpableWall = false;
var movementScale = 1
var type = 'salt'
var drowned = false
class_name PlayerCharacter

signal death_occurred
signal win

const explosion = preload("res://Scenes/Particles/Explosion.tscn")
const sulphurExplosion = preload("res://Scenes/StageComponents/Player/SulphurExplosion.tscn")
const winningPlayer = preload("res://Scenes/StageComponents/Player/WinningPlayer.tscn")

func process_sprite():
	if not is_on_floor():
		$Sprite.play(type + '_jump')
		if (velocity.y < 200):
			if (velocity.y < -200):
				$Sprite.set_frame(0)
			else:
				$Sprite.set_frame(1)
		else:
			if (velocity.y < 600):
				$Sprite.set_frame(2)
			else:
				$Sprite.set_frame(3)
	else:
		if (not abs(velocity.x) < 100):
			$Sprite.play(type + '_run')
		else:
			$Sprite.play(type + '_squish')

func changeType(typeTo):
	var explosionEffect = explosion.instance()
	explosionEffect.scale = Vector2(2, 2)
	explosionEffect.position.y = -8
	explosionEffect.lifetime = 0.3
	explosionEffect.remove_particle()
	call_deferred("add_child", explosionEffect)
	type = typeTo
	if (typeTo == 'brimstone_charged'):
		$TrailParticle.emitting = true
	else:
		$TrailParticle.emitting = false

func drown():
	if (type == 'salt'):
		changeType('salt_purified')
	elif (type == 'brimstone' or type == 'brimstone_charged'):
		kill()
	drowned = true
	gravMult = 0.8
	terminalVelocity = 400

func undrown():
	drowned = false
	gravMult = 1
	terminalVelocity = 1000
	

func kill():
	if (!living):
		return
	living = false
	$Sprite.visible = false
	$WalkParticle.queue_free()
	$DeathTimer.start()
	var explosionEffect = explosion.instance()
	explosionEffect.scale = Vector2(3, 3)
	explosionEffect.lifetime = 0.3
	call_deferred("add_child", explosionEffect)
	play_death_sound()
	emit_signal("death_occurred")

func brimstoneDischarge():
	if (type == 'brimstone_charged'):
		$DepowerPlayer.play()
		var explosion = sulphurExplosion.instance()
		explosion.position = position
		get_tree().get_root().call_deferred("add_child", explosion)
		changeType('brimstone')

func win():
	living = false
	var winning = winningPlayer.instance()
	winning.position = position
	winning.velocity = velocity
	winning.setSpriteType(type)
	winning.player = self
	get_parent().call_deferred('add_child', winning)
	# get_parent().add_child(winning)
	$Sprite.visible = false
	emit_signal('win')

func play_jump_sound():
	if !$JumpPlayer.is_playing():
		$JumpPlayer.stop()
	$JumpPlayer.play()

func play_death_sound():
	if !$DeathPlayer.is_playing():
		$DeathPlayer.stop()
	$DeathPlayer.play()

# =============================================================================
# Process functions
# =============================================================================

func _process(_delta):
	process_sprite()

func _physics_process(delta):
	if (not living):
		return
	# Horizontal speed physics ================================================
	if (type == 'brimstone_charged'):
		if (abs(velocity.x) < 100):
			brimstoneDischarge()
		else:
			velocity.x = 800 * movementScale
	else:
		velocity.x = clamp(
			velocity.x + delta * horizontalAccel * movementScale,
			-maxHorizontalSpeed,
			maxHorizontalSpeed)

	# Jump physics ============================================================
	if is_on_ceiling():
		brimstoneDischarge()
		$JumpHoldTimer.stop()

	var t = 1 - (
		$JumpHoldTimer.time_left / $JumpHoldTimer.wait_time)
	var apparent_gravity = lerp(
		jumpGravityMultiplication * gravity,
		gravity,
		1 - cos(PI / 2 * t)
	) * gravMult

	velocity.y = clamp(
		velocity.y + delta * apparent_gravity,
		-terminalVelocity,
		terminalVelocity)
		
	if (is_on_floor() or touchingJumpableWall):
		$JumpInputManager.set_able_to_jump()
	else:
		$JumpInputManager.set_unable_to_jump()
	
	if (is_on_floor() and abs(velocity.x) > maxHorizontalSpeed * 0.8):
		$WalkParticle.emitting = true
	else:
		$WalkParticle.emitting = false
	
	if (beginJump):
		beginJump = false
		velocity.y = jumpSpeed
		if (touchingJumpableWall and not is_on_floor()):
			$Sprite.scale.x *= -1
			movementScale *= -1
			$FaceDetector.scale.x *= -1
			velocity.x = maxHorizontalSpeed * movementScale * 0.8

	# Fix one way jank by max.marauder
	# https://godotengine.org/qa/76431/player-character-occasionally-bounces-collision-platforms
	var alteredVelocity = move_and_slide(velocity, UP, true)
	if (alteredVelocity.y == 0) && (velocity.y < 0) && is_on_floor():
		alteredVelocity.y = velocity.y
	velocity = alteredVelocity
	if (position.y > 2000):
		kill()

func _ready():
	$Camera.current = true
# =============================================================================
# Signals
# =============================================================================

func _on_JumpInputManager_grounded_jump_press():
	play_jump_sound()
	$JumpHoldTimer.start()
	beginJump = true

func _on_JumpInputManager_jump_release():
	$JumpHoldTimer.stop()
	beginJump = false

func _on_HeadDetector_body_entered(body):
	if (body is SwitchBlock):
		body.bump(body.world_to_map(position) + UP)

func _on_FaceDetector_body_entered(body):
	if (body is JumpableWall):
		touchingJumpableWall = true

func _on_FaceDetector_body_exited(body):
	if (body is JumpableWall):
		touchingJumpableWall = false

func _on_Hurtbox_body_entered(body):
	if (body.is_in_group('hurting')):
		kill()

func _on_DeathTimer_timeout():
	queue_free()


func _on_JumpInputManager_airborne_jump_press():
	if (type == 'salt_purified'):
		$JumpHoldTimer.start()
		beginJump = true
		play_jump_sound()
		if !drowned:
			changeType('salt')
