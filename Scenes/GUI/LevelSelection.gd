extends CanvasLayer

var selectedLevel = 0

signal playStage

const LevelButton = preload('res://Scenes/GUI/LevelSelectionButton.tscn')
var levelNames

var buttons = []

func _ready():
	for i in range(len(levelNames)):
		var button = LevelButton.instance()
		$ItemList.add_child(button)
# warning-ignore:integer_division
		button.setup(['none', 'fire', 'water'][i / 4], i + 1)
		buttons.append(button)
	updateSelection(selectedLevel)

func updateSelection(d):
	buttons[selectedLevel].unhighlight()
	selectedLevel += d + 8
	selectedLevel %= 8
	
#	if selectedLevel >= 10:
#		if abs(d) == 1:
#			selectedLevel %= 10
#		else:
#			selectedLevel = (selectedLevel + 4) % 12
	buttons[selectedLevel].highlight()
	
	$LevelName.text = '%d - %s' % [selectedLevel + 1, levelNames[selectedLevel]]

func play_sound():
	if $Sound.playing:
		$Sound.stop()
	$Sound.play()

func _process(_delta):
	if Input.is_action_just_pressed("ui_up"):
		play_sound()
		updateSelection(-4)
	if Input.is_action_just_pressed("ui_down"):
		play_sound()
		updateSelection(+4)
	if Input.is_action_just_pressed("ui_right"):
		play_sound()
		updateSelection(+1)
	if Input.is_action_just_pressed("ui_left"):
		play_sound()
		updateSelection(-1)
	if Input.is_action_just_pressed("ui_accept"):
		play_sound()
		emit_signal('playStage', selectedLevel)
	selectedLevel %= 8
